#!/usr/bin/python3
#    latex2sympy
#    Copyright (C) 2018 Volker Weissmann . Contact: volker.weissmann@gmx.de

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.

#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

from sympy2foam import *
import os

def assertGoodReturn(src):
	with open("testSympy2foam.cpp","w") as outfile:
		outfile.write("#include <cassert>\n#include \"openfoamcpp.cpp\"\nusing namespace Foam;\nint main(){\n")
		outfile.write(src)
		outfile.write("\n}")
	os.system("g++ testSympy2foam.cpp -o testSympy2foam")
	retc = os.system("./testSympy2foam  2>/dev/null")
	assert retc==0, src
	os.remove("testSympy2foam.cpp")
	os.remove("testSympy2foam")

def assertBadReturn(src):
	with open("testSympy2foam.cpp","w") as outfile:
		outfile.write("#include <cassert>\n#include \"openfoamcpp.cpp\"\nusing namespace Foam;\nint main(){\n")
		outfile.write(src)
		outfile.write("\n}")
	os.system("g++ testSympy2foam.cpp -o testSympy2foam")
	retc = os.system("./testSympy2foam  2>/dev/null")
	assert retc!=0, src
	os.remove("testSympy2foam.cpp")
	os.remove("testSympy2foam")

def testPiecewise():
	foam = sympy2foam(latex2sympy(r"c\mapsfrom\begin{cases}17&x<4\\18&x>5\end{cases}"))
	src = "{scalar codegen_x = 3.7;"+foam+"assert(codegen_c == 17);}"  +"{scalar codegen_x = 5.7;"+foam+"assert(codegen_c == 18);}"
	assertGoodReturn(src)
	src = "scalar codegen_x = 4.7;"+foam
	assertBadReturn(src)

testPiecewise()

#print(sympy2foam(latex2sympy(r"c\mapsfrom\begin{cases}17&x<4\\18&x>5\end{cases}")))