#!/usr/bin/python3
import random
import sympy
from latex2sympy import *

class TestFailed(Exception):
    pass
def testIfSameSympyFunc(exprs): #exprs: array aus sympy expressions
    #print("testIfSameSympyFunc")
    #print(exprs)
    syms = exprs[0].free_symbols
    for i in exprs[1:]:
        if syms != i.free_symbols:
            print("Different free symbols")
            raise TestFailed
    #print("same free symbols")
    for i in range(0,20):
        xyz = [random.random() for i in syms]
        sub = dict(zip(syms,xyz))
        value = exprs[0].subs(sub)
        for e in exprs[1:]:
            if abs(value-e.subs(sub)) > 1e-6:
                raise TestFailed
            #if isinstance(value, sympy.Float):
            #    if abs(value-e.subs(sub)) > 1e-6:
            #        raise TestFailed
            #elif value != e.subs(sub):
            #    raise TestFailed
    #print("success in testIfSameSympyFunc")

def testSympy2Latex2Sympy(exprs):
    for expr in exprs:
        failed = False
        try:
            testIfSameSympyFunc([expr, latex2sympy(sympy.latex(expr), ConfigL2S())])
        except ParsingError:
            print("Parsing failed:", sympy.latex(expr))
            failed = True
        except TestFailed:
            print("Test failed: Sympy:", expr, "Latex:", sympy.latex(expr), "Sympy:", latex2sympy(sympy.latex(expr), ConfigL2S()))
            failed = True
        if failed:
            try:
                latex2sympy(sympy.latex(expr), ConfigL2S(DEBUG=True))
            except:
                pass
def testLatexSympyPairs(pairs):
    for pair in pairs:
        failed = False
        try:
            testIfSameSympyFunc([pair[1], latex2sympy(pair[0], ConfigL2S())])
        except ParsingError:
            print("Parsing failed:", pair[0])
            failed = True
        except TestFailed:
            print("Test failed: Latex:", pair[0], "Sympy:", pair[1], latex2sympy(pair[0], ConfigL2S()))
            failed = True
        if failed:
            try:
                latex2sympy(pair[0], ConfigL2S(DEBUG=True))
            except ParsingError:
                pass
def testFindEndOfIndexOrOperandPairs(pairs):
    for pair in pairs:
        if findEndOfIndexOrOperand(pair[0], 0) != pair[1]:
            print("Test failed:", pair[0], pair[1], findEndOfIndexOrOperand(pair[0], 0))
findEndOfIndexOrOperandPairs = [
    ("ab", 1),
    ("{b}c", 3),
    ("\\text{abc}", 10),
    ("\\text {abc}", 11),
]
testFindEndOfIndexOrOperandPairs(findEndOfIndexOrOperandPairs)
sympyStr = [
    "a+b/c",
    "a**3",
]
testSympy2Latex2Sympy([strToSympy(s) for s in sympyStr])
latexSympyStrPairs = [
    ("a+bc", "a+b*c"),
    ("a_b^4", "(a_b)**4"),
    ("a^4_b", "(a_b)**4"),
    ("(a+b)^3", "(a+b)**3"),
    ("-a_2+b", "-a_2+b"),
    ("a/b+2", "a/b+2"),
    ("2*(a+b)+c", "2*(a+b)+c"),
    ("2*\\left(a+b\\right)+c", "2*(a+b)+c"),
    #("\\sin(a)", "sin(a)"),
    #("\\exp(a)", "exp(a)"),
    #("\\ln(a)", "ln(a)"),
    ("\\begin{cases}1&x>0.5\\\\0&x\\leq0.5\\end{cases}", "Piecewise((1.0, x > 0.5), (0, True))"),
    ("\\sqrt{a}", "sqrt(a)"),
    ("\\sqrt[1/3]{a}", "a**3"),
    ("\\sqrt{1/2}", "sqrt(1/2)"),
    ("\\sqrt{1/2}+a", "sqrt(1/2)+a"),
    ("a+\\left(b+c\\right)", "a+b+c"),
    #("a%b", "a"),
	(r"\sin(x)", "sin(x)"),
]
testLatexSympyPairs([(lat,strToSympy(spy)) for lat,spy in latexSympyStrPairs])
