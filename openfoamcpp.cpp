#include <iostream>
#include <sstream>
#include <fstream>
#include <vector>
#include <cstdlib>
#include <cmath>
#include <utility>
#define DEBUG_VAR(x) std::cout << #x << ": " << x << nl
#define FatalErrorInFunction std::cout
#define FatalError 1
#define endl '\n'
#define nl '\n'
#define abs(x) ( (x) > 0 ? (x) : (-(x))  )
#define min(a,b) ( (a) < (b) ? (a) : (b) )
#define max(a,b) ( (a) > (b) ? (a) : (b) )
#define sqr(x) ((x)*(x))
#define mag(vec) sqrt(magSqr(vec))
#define magSqr(vec) (sqr(vec.x)+sqr(vec.y)+sqr(vec.z))
const double pi = M_PI;
int abort(int error) {
    std::cout << "aborting..." << nl;
    exit(error);
    return error;
}
namespace Foam
{
    int exit(int error) {
        std::cout << "aborting..." << nl;
        exit(error);
        return error;
    }
    typedef double scalar;
    typedef int label;
    class vector {
    public:
        scalar x,y,z;
        vector(scalar x, scalar y, scalar z) {
            this->x = x;
            this->y = y;
            this->z = z;
        }
        scalar operator&(const vector& rhs) const {
            return x*rhs.x + y*rhs.y + z*rhs.z;
        }
        /*vector& operator=(const vector& other) {
            this->x = other.x;
            this->y = other.y;
            this->z = other.z;
            return *this;
            }*/
        friend vector operator*(const vector lhs, const vector rhs) {
            return vector(lhs.y*rhs.z - lhs.z*rhs.y , lhs.z*rhs.x - lhs.x*rhs.z , lhs.x*rhs.y - lhs.y*rhs.x);
        }
        friend vector operator*(const vector lhs, const scalar rhs) {
            return vector(lhs.x*rhs, lhs.y*rhs, lhs.z*rhs);
        }
        friend vector operator*(const scalar lhs, const vector rhs) {
            return vector(rhs.x*lhs, rhs.y*lhs, rhs.z*lhs);
        }
        friend vector operator/(const vector lhs, const scalar rhs) {
            return vector(lhs.x/rhs, lhs.y/rhs, lhs.z/rhs);
        }
        friend vector operator/(const scalar lhs, const vector rhs) {
            return vector(rhs.x/lhs, rhs.y/lhs, rhs.z/lhs);
        }
        friend vector operator+(const vector lhs, const vector rhs) {
            return vector(lhs.x+rhs.x, lhs.y+rhs.y, lhs.z+rhs.z);
        }
        friend vector operator-(const vector lhs, const vector rhs) {
            return vector(lhs.x-rhs.x, lhs.y-rhs.y, lhs.z-rhs.z);
        }
        void print() const {
            std::cout << this->x << " " << this->y << " " << this->z << nl;
        }
        scalar component(const int i) {
            switch (i) {
                case 0: return x;
                case 1: return y;
                case 2: return z;
            }
            std::cout << "invalid index\n";
            exit(1);
        }
    };
    template<typename T>
    class List : public std::vector<T> {
        using std::vector<T>::vector;
    };
    class Particle {
    public:
        scalar mass_; scalar mass() { return mass_; };
        scalar rho_; scalar rho() { return rho_; };
        scalar T_; scalar T() { return T_; };
    };
    class Wall {
    public:
        scalar critTemp_; scalar critTemp() { return critTemp_; };
        scalar surfaceEnergy_; scalar surfaceEnergy() { return surfaceEnergy_; };
        scalar impulseRatio_; scalar impulseRatio() { return impulseRatio_; };
        scalar wallPoisson_; scalar wallPoisson() { return wallPoisson_; };
        scalar particlePoisson_; scalar particlePoisson() { return particlePoisson_; };
        scalar wallYoungsModulus_; scalar wallYoungsModulus() { return wallYoungsModulus_; };
        scalar particleYoungsModulus_; scalar particleYoungsModulus() { return particleYoungsModulus_; };
        std::vector<double> taylorAvT_; std::vector<double> taylorAvT() { return taylorAvT_; };
        scalar constSymA_; scalar constSymA() { return constSymA_; };
        scalar velocityLimit_; scalar velocityLimit() { return velocityLimit_; };
        scalar yieldStress_; scalar yieldStress() { return yieldStress_; };
    };
}
