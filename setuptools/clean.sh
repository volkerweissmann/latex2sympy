cd ${0%/*} || exit 1
rm -r build
rm -r dist
rm -r src
rm -r latex2sympy
rm -r latex2sympy.egg-info
rm LICENSE.txt
rm README.md
