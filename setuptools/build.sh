cd ${0%/*} || exit 1
mkdir latex2sympy
cp ../latex2sympy.py latex2sympy/__init__.py
cp ../LICENSE.txt LICENSE.txt
cp ../README.md README.md
python3 setup.py sdist bdist_wheel
