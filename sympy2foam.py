#!/usr/bin/python3
import sympy
from latex2sympy import *

def variableNameConvert(name):
    return "codegen_" + name.replace("\\text", "").replace("{", "").replace("}", "").replace("\\", "")
def vecOrScalar(expr):
    #print(type(expr))
    #print(expr)
    if isinstance(expr, sympy.symbol.Symbol):
        if str(expr).startswith("vec_"):
            return True
        return False
    elif isinstance(expr, (sympy.numbers.NegativeOne,sympy.numbers.Integer,sympy.numbers.Float)):
        return False
    elif str(type(expr)) == "codegen_operatorname_mag":
        return False
    elif isinstance(expr, sympy.power.Pow):
        if not vecOrScalar(expr.args[0]):
            return False
        if expr.args[1] % 2 == 0:
            return False
        return True
    elif isinstance(expr, sympy.mul.Mul):
        ret = False
        for a in expr.args:
            if vecOrScalar(a):
                ret = not ret
        return ret
    elif isinstance(expr, sympy.add.Add):
        ret = vecOrScalar(expr.args[0])
        for i in range(1,len(expr.args)):
            if vecOrScalar(expr.args[i]) != ret:
                raise ValueError("Additon of a vector and a scalar")
        return ret
    raise ValueError("Unsupported in vecOrScalar")
specialSympyNumberTypes = [sympy.numbers.Zero,
                           sympy.numbers.One,
                           sympy.numbers.NegativeOne,
                           sympy.numbers.Half,
                           sympy.numbers.Infinity,
                           sympy.numbers.NegativeInfinity,
                           sympy.numbers.ComplexInfinity,
                           sympy.numbers.Exp1,
                           sympy.numbers.ImaginaryUnit,
                           sympy.numbers.Pi,
                           sympy.numbers.EulerGamma,
                           sympy.numbers.Catalan,
                           sympy.numbers.GoldenRatio,
                           sympy.numbers.TribonacciConstant]
sympyNumberTypes = [sympy.numbers.Float , sympy.numbers.Rational, sympy.numbers.Integer]
sympyFoamFunctionMap = { #Only for functions with one argument, pow is handled below
    sympy.cos: "cos",
    sympy.sin: "sin",
    sympy.tan: "tan",
    sympy.acos: "acos",
    sympy.asin: "asin",
    sympy.atan: "atan",
    sympy.cosh: "cosh",
    sympy.sinh: "sinh",
    sympy.tanh: "tanh",
    sympy.acosh: "acosh",
    sympy.asinh: "asinh",
    sympy.atanh: "atanh",
    
    sympy.exp: "exp",
    sympy.log: "log",
    sympy.sqrt: "sqrt",
    sympy.cbrt: "cbrt",
    #See http://www.cplusplus.com/reference/cmath/ and https://docs.sympy.org/latest/modules/functions/elementary.html
}
def sympyTerm2foam(expr):
    if type(expr) == sympy.symbol.Symbol:
        return variableNameConvert(str(expr))
    elif type(expr) == sympy.mul.Mul:
        ret = "("
        #for a in expr.args:
        #    if ret != "":
        #        ret += " * "
        #    ret += "(" + sympyTerm2foam(a) + ")"
        ret += "(" + sympyTerm2foam(expr.args[0]) + ")"
        for i in range(1, len(expr.args)):
            if vecOrScalar(expr.args[i-1]) and vecOrScalar(expr.args[i]):
                ret += " & "
            else:
                ret += " * "
            ret += "(" + sympyTerm2foam(expr.args[i]) + ")"
        ret += ")"
        return ret
    elif type(expr) == sympy.add.Add:
        ret = ""
        for a in expr.args:
            if ret != "":
                ret += " + "
            ret += sympyTerm2foam(a)
        return ret
    elif type(expr) == sympy.power.Pow:
        return "pow(" + sympyTerm2foam(expr.args[0]) + "," + sympyTerm2foam(expr.args[1]) + ")"
    elif type(expr) in sympyFoamFunctionMap:
        assert(len(expr.args)==1)
        return sympyFoamFunctionMap[type(expr)] + "(" + sympyTerm2foam(expr.args[0]) + ")"
    elif type(expr) in sympyNumberTypes or type(expr) in specialSympyNumberTypes:
        ret = str(float(expr))
        if "." not in ret:
            ret += ".0"
        return ret
    elif type(expr) == sympy.boolalg.BooleanTrue:
        return "true"
    elif type(expr) == sympy.GreaterThan:
        return sympyTerm2foam(expr.args[0]) + " >= " + sympyTerm2foam(expr.args[1])
    elif type(expr) == sympy.LessThan:
        return sympyTerm2foam(expr.args[0]) + " <= " + sympyTerm2foam(expr.args[1])
    elif type(expr) == sympy.StrictGreaterThan:
        return sympyTerm2foam(expr.args[0]) + " > " + sympyTerm2foam(expr.args[1])
    elif type(expr) == sympy.StrictLessThan:
        return sympyTerm2foam(expr.args[0]) + " < " + sympyTerm2foam(expr.args[1])
    elif type(expr) == sympy.Piecewise:
        ret = ""
        for i in range(0, len(expr.args)):
            ret += "(" + sympyTerm2foam(expr.args[i][1]) + ") ? (" + sympyTerm2foam(expr.args[i][0]) + ") : ";
        ret += "(throw std::domain_error(\"Non exhaustive case in sympy2foam\"))"
        # These four lines work, but wont catch non exhaustive cases
        # ret = ""
        # for i in range(0, len(expr.args)-1):
        #     ret += "(" + sympyTerm2foam(expr.args[i][1]) + ") ? (" + sympyTerm2foam(expr.args[i][0]) + ") : ";
        # ret += "(" + sympyTerm2foam(expr.args[-1][0]) + ")"
        return ret
    elif str(type(expr)).startswith("codegen_operatorname_"):
        return str(expr.func)[21:] + "(" + sympyTerm2foam(expr.args[0])  + ")"
    else:
        print("unknown type:", type(expr))
        print(expr)
        exit(1)
def sympy2foam(expr):
    if not isinstance(expr, sympy.codegen.ast.Assignment):
        print("bad type for sympy2foam")
        exit(1)
    src = ""
    if str(expr.lhs).startswith("vec_"):
        src += "const vector " + variableNameConvert(str(expr.lhs))
    else:
        src += "const scalar " + variableNameConvert(str(expr.lhs))
    src += " = "
    src += sympyTerm2foam(expr.rhs)
    src += ";"
    return src


#sympy2foam(sympy.simplify(latex2sympy("V \\mapsfrom \\frac{m}{\\rho}", ConfigL2S())))
#sympy2foam(sympy.simplify(latex2sympy("l \\mapsfrom \\sqrt[3]{\\frac{16}{9\\pi}V}", ConfigL2S())))

def convertFile(inpath, outpath):
    #print(sympyTerm2foam(sympy.sympify("codegen_vec_a*codegen_vec_b")))
    #print(sympy2foam(latex2sympy("c_s \\mapsfrom \\frac{\\bm U_{t1}\\cdot\\bm U_{t1}}{U_{n1}^2+\\bm U_{t1}\\cdot\\bm U_{t1}}",ConfigL2S(DEBUG=False))))
    #print(sympy2foam(latex2sympy("c_s \\mapsfrom \\bm U_{t1}\\cdot\\bm U_{t1}",ConfigL2S(DEBUG=False))))
    #exit(0)
    outfile = open(outpath, "w")
    f = open(inpath, "r")
    input = f.read()
    equations = []
    i = 0
    casestate = False
    equationstate = False
    while i < len(input):
        if input[i:i+13] == "\\begin{cases}":
            casestate = True
        if input[i:i+11] == "\\end{cases}":
            casestate = False
        if input[i:i+14] == "\\begin{align*}":
            equationstate = True
            equations.append("")
            i += 13
        elif input[i:i+12] == "\\end{align*}":
            equationstate = False
            i += 11
        elif input[i:i+2] == "\\\\" and not casestate and equationstate:
            equations.append("")
            i += 1
        elif equationstate:
            equations[-1] += input[i]
        i += 1
    equations = [eq.replace("\n", " ") for eq in equations]
    equations = [eq for eq in equations if not eq.isspace()]
    equations = [latex2sympy(eq,ConfigL2S(DEBUG=False)) for eq in equations]
    outfile.write("/*----------begin-of-codegen---------------*/\n")
    for eq in equations:
        outfile.write(sympy2foam(eq))
        outfile.write("\n")
    outfile.write("/*------------end-of-codegen---------------*/\n")
